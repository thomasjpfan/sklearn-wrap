## v2020.6.24

* Add 'auto' option for Hyperparameter `multi_class` in SKLogisticRegression, defaults to 'auto'
* Fix #254: Change the Param type for stop_words_ to 'Any' in SKCountVectorizer and SKTfidfVectorizer 
* Fix #252: Ensure input_column_names to be str column types

## v2020.6.10

* Fix #250: The intercept_ of SKLogisticRegression should be float, integer, or ndarray
* Fix #251: add workaround for random state on primitives that do not expose it (SVC/SVR)
* Fix #331 from primitives-repo: Hyperparameter `add_indicator` should be a ControlParam instead of TuningParam
* Fix #352 from primitives-repo: Feature selection Primitives returns a regular list, now returns d3m container

## v2020.5.26

* Fix #227: Use Pandas sparse columns for sklearn estimators which otherwise return sparse data
* Fix #248: Add CPU_resource parameter to n_jobs
* Fix #247: Bump scikit-learn to 0.22
   * OneHotEncoder: `n_values` hyperparameter depreciated
   * RandomForestRegressor: adding `ccp_alpha` hyperparameter
   * DecisionTreeRegressor: adding `ccp_alpha` hyperparameter
   * ExtraTreesRegressor: adding `max_samples` hyperparameter
   * GradientBoostingRegressor: adding `ccp_alpha` hyperparameter
   * MLPRegressor: adding `max_fun` hyperparameter
* Fix #233-245: update lower_inclusive and upper_inclusive values
   * RandomForestClassifier, KNeighborsClassifier, DecisionTreeClassifier, ExtraTreesClassifier, GradientBoostingClassifier,
   BaggingClassifier, RandomForestRegressor, KNeighborsRegressor, DecisionTreeRegressor, ExtraTreesRegressor, 
   BaggingRegressor, MLPRegressor, MLPClassifier
* Fix #353 in primitives repo: change MLPClassifier/Regressor max_iter from float to int

## v2019.11.13

* skip mutual_info_classif/mutual_info_regression
* Fix #229: Fix StringImputer Primitive and Pipeline
* Fix #226: LabelSpreading and LabelPropagation primitive added for semi-supervised learning support
* Fix #228: Update scikit learn dependency to 21.3. Support for latest d3m core package
* Fix #145: Remove `better_f_regression`. It seems `better_f_regression` is the same as `f_regression`
* Fix #112: Overlays should be moved to different folder
* PartialFix #222: Fix SparseRandomProjection, MissingIndicator, OrdinalEncoder, GaussianRandomProjection pipelines

## v2019.6.7

* Fix #220: Don't copy structural type from input
* Fix #192: Move computation from `set_training_data` to `fit`. `oob_score` only set when `bootstrap` set.
* Fix #212: All predictors to select what semantic type to add on output
* Fix #37:  Requst for sklearn mutual information primitives
* Fix #173: Add StringImputer to support strings as inputs for Imputer
* Fix #36:  Add short-lists of tunning hyper-parameters to `hyperparams_to_tune` metadata
* Fix #111: Transition to `Choice` hyper-params
* Fix #217: Set accepted semantic types in ordinal encoder to only accept `Attribute`
* Fix #183: Add SparseRandomProjection and GaussianRandomProjection
* Fix #151: Test all produce methods in generated pipelines
* Fix #93:  Add More Sklearn Primitives: sklearn.linear_model.LinearRegression, sklearn.ensemble.BaggingRegressor
* Fix #219: Rename use_input_columns to use_inputs_columns
* Fix #218: OrdinalEncoder does not work on string columns
* Fix #185: Semantic types of children hyper-parameters same as parent
* Fix #211: Improve testing framework and code
* Fix #216: Remove skip from adaboost classifier/regressor
* Fix #180: Ask for a sklearn primitive: sklearn.preprocessing.QuantileTransformer
* Fix #176: Expose continue_fit on learners haveing partial_fit method
* Fix #189: Allow feature construction primitives to output columns with semantic type ConstructedAttribute
* Fix #156: Feature selection primitives should have different return_result values

## v2019.4.4

* Fix #198: SKOneHotEncoder should also allow encoding target columns
* Fix #208: Metadata incorrect when return_result set to append
* Fix #202: Update tests to use overlay files to check for semantic types
* Fix #210: Move Kmeans to overlay_unsupervised
* Fix #203: All hyper-parameters which are currently defined as List[X] type, should be rather a List hyper-parameter
* Fix #135: Make ngram_range two hyper-parameters in SKCountVectorizer
* Fix #191: Better describe hidden_layer_sizes in SKMLPClassifier and SKMLPRegressor
* Fix #209: Use constant hyper-parameter instead of hyperparams.Hyperparameter[None]
* Fix #117: Random Forest wrong annotation
* Fix #199: Address warnings while sklearn pipelines are being run
* Fix #206: In random forest, class_weight can be balanced or sub_sample
* Fix #177: Add sklearn.linear_model.ElasticNet in the repo
* Fix #201: Remove DictVectorizer primitive
* Fix #197: SKOneHotEncoder should work only on columns with semantic type Categorical and Attribute
* Fix #171: Overlay files should be able to control what semantic types to add/remove in a primitive
* Fix #188: PCA does not select proper columns to operate on.
* Fix #89.6: You can list hyperparams_to_tune: max_leaf_nodes, criterion, max_features.
* Fix #190: Use baseball dataset in pipelines instead of 38_sick
* Fix #187: Dont provide primitive digest in pipelines
* Fix #89.8: log_likelihoods is broken and does not work with non-numeric targets (outputs).
* Fix #175: In Imputer, for columns which are skipped because they are all null, we have to do something else than just ignoring them
* Fix #174: In Imputer, removing columns which are completely NaN should happen only if strategy is not "constant"
* Fix #181: Don't include "v" in package version
* Fix #179: Primitives should raise PrimitiveNotFittedError
* Fix #74: Update primitive family for PCA and KPCA to `FEATURE_EXTRACTION`
* Fix #98: Ordinal encoder can now operate on `../CategoricalData` semantic type
    * This means the the primitive will check for the presence of CategoricalData and nothing else
    * This fix also includes a fix to the preprocessors not copying input metadata to produced columns
* Fix #159: Do not drop column with all NaNs in SKImputer
* Fix #168: Wrap MLPClassifier as well
* Fix #167 and #168: Sklearn wrap has a single imputer
    * The Simple Imputer in sklearn is wrapped under d3m.primitives.data_cleaning.imputer.SKlearn with the same ID as the previoud imputer
* Fix #170: Update sklearn to use utils from base package instead of common primitives
* Fix #137: Issue warning if no inputs were selected
* Fix #157 and #158
    * Make feature importances return features as columns and importances as row
    * Make sure produce_cluster_centers returns features as columns and centers as rows
* Fix #137: When using semantic types, primitives should not fail if no column is selected

## v2019.2.27

* Changed Primitive Interfaces:
    * StandardScaler: SupervisedLearnerPrimitiveBase  --> UnsupervisedLearnerPrimitiveBase
    * PolynomialFeatures: SupervisedLearnerPrimitiveBase --> UnsupervisedLearnerPrimitiveBase
    * VarianceThreshold: TransformerPrimitiveBase --> SupervisedLearnerPrimitiveBase

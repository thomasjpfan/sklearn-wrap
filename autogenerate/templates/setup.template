import os
from setuptools import setup, find_packages

PACKAGE_NAME = '{{ package_name }}'


def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    assert False, "'{0}' not found in '{1}'".format(key, module_path)


setup(
    name=PACKAGE_NAME,
    version=read_package_variable('__version__'),
    description='Primitives created using the Sklearn auto wrapper',
    author=read_package_variable('__author__'),
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=[
        'd3m',
        'scikit-learn',
    ],
    url='https://gitlab.datadrivendiscovery.org/jpl/sklearn-wrapping',
    entry_points = {
        'd3m.primitives': [{% for i in range(python_paths| length) %}
            '{{ python_paths[i].split('d3m.primitives.', 1)[1].rstrip('\"')  }} = {{ package_name }}.{{ classes[i] }}:{{ classes[i] }}',{% endfor %}
        ],
    },
)

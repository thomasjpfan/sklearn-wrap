# To generate an overlay template for all primitives in the primitives directory

import os, os.path, simplejson, sys
from autogenerate import parse_code

primitives_dir = os.path.join(os.path.dirname(__file__), "resources", "primitive-jsons")


def get_mapped_type(type):
    mappings = {
        'int': 'UniformInt',
        'float': 'Uniform'
    }
    for key, value in mappings.items():
        if key in type:
            return value
    return 'Hyperparameter'


def get_mapped_structural_type(type):
    mappings = {
        "string": "str",
        "integer": "int",
        "boolean": "bool"
    }
    return mappings.get(type, type)


def generate_template(primitive, primitive_family=""):
    template = dict()
    template['class_name'] = primitive.get("name")
    template['algorithm_types'] = []
    template['primitive_family'] = primitive_family
    template['Hyperparams'] = []
    for param in primitive.get("parameters"):
        hyperparam_base = {
            "type": "",
            "name": "",
            "init_args":{
                "default":""
            }
        }
        hyperparam_base['name'] = param.get("name")
        hyperparam_base['type'] = get_mapped_type(param.get('type'))
        hyperparam_base['init_args']['semantic_types'] = [param.get("name")]
        hyperparam_base['init_args']['default'] = param.get("default", "")
        hyperparam_base['init_args']['_structural_type'] = get_mapped_structural_type(param.get('type'))
        template['Hyperparams'].append(hyperparam_base)
        if 'Uniform' in hyperparam_base['type']:
            hyperparam_base['init_args']['lower'] = ""
            hyperparam_base['init_args']['upper'] = ""
    return template


def verify_template(class_name, overlay, primtive):
    """
    Verifies if the names, types of Hyperparams and params, if any, match with the newly parsed code.
    :param primitive:
    :param overlay:
    :return: Bool
    """
    overlay_dict = overlay.get(class_name)


def get_classname(name):
    # "name": "sklearn.ensemble.forest.RandomForestClassifier"
    name = name.split(".")[-1]
    return name


def main(args):
    qual_class_name = args[1]
    overlay_file = args[2]
    overlay = simplejson.load(open(overlay_file, 'r'))
    primitive = parse_code.generate_primitive_annotation(qual_class_name)
    class_name = qual_class_name.split(".")[-1]
    if class_name not in overlay:
        overlay.update({class_name: generate_template(primitive, overlay.get('primitive_family'))})
        with open(overlay_file, 'w') as fw:
            fw.write(simplejson.dumps(overlay, indent=2))
        with open(os.path.join(primitives_dir, "{}.json".format(class_name)), 'w') as fw:
            fw.write(simplejson.dumps(primitive, indent=2))
    else:
        with open(os.path.join(primitives_dir, "{}.json".format(class_name)), 'w') as fw:
            fw.write(simplejson.dumps(primitive, indent=2))


if __name__ == '__main__':
    args = sys.argv
    if len(args) < 3:
        print("Please provide input params <sklearn class name> <overlay file>\n"
              "sklearn class name - qualified class name ex- sklearn.feature_extraction.text.TfidfVectorizer\n"
              "path to overlay file - The overlay file in which the class should belong ex - autogenerate/resources/overlay_preprocessors.json")
        exit(1)
    main(args)


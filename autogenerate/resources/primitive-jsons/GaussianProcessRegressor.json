{
  "is_class": true, 
  "library": "sklearn", 
  "compute_resources": {}, 
  "common_name": "Gaussian Process Regressor", 
  "id": "25e331f8-9100-35fd-baf6-b88555580d35", 
  "category": "gaussian_process.gpr", 
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/gaussian_process/gpr.py#L20", 
  "parameters": [
    {
      "type": "kernel", 
      "name": "kernel", 
      "description": "The kernel specifying the covariance function of the GP. If None is passed, the kernel \"1.0 * RBF(1.0)\" is used as default. Note that the kernel\\'s hyperparameters are optimized during fitting. "
    }, 
    {
      "type": "float", 
      "optional": "true", 
      "name": "alpha", 
      "description": "Value added to the diagonal of the kernel matrix during fitting. Larger values correspond to increased noise level in the observations and reduce potential numerical issue during fitting. If an array is passed, it must have the same number of entries as the data used for fitting and is used as datapoint-dependent noise level. Note that this is equivalent to adding a WhiteKernel with c=alpha. Allowing to specify the noise level directly as a parameter is mainly for convenience and for consistency with Ridge. "
    }, 
    {
      "type": "string", 
      "optional": "true", 
      "name": "optimizer", 
      "description": "Can either be one of the internally supported optimizers for optimizing the kernel\\'s parameters, specified by a string, or an externally defined optimizer passed as a callable. If a callable is passed, it must have the signature::  def optimizer(obj_func, initial_theta, bounds): # * \\'obj_func\\' is the objective function to be maximized, which #   takes the hyperparameters theta as parameter and an #   optional flag eval_gradient, which determines if the #   gradient is returned additionally to the function value # * \\'initial_theta\\': the initial value for theta, which can be #   used by local optimizers # * \\'bounds\\': the bounds on the values of theta .... # Returned are the best found hyperparameters theta and # the corresponding value of the target function. return theta_opt, func_min  Per default, the \\'fmin_l_bfgs_b\\' algorithm from scipy.optimize is used. If None is passed, the kernel\\'s parameters are kept fixed. Available internal optimizers are::  \\'fmin_l_bfgs_b\\' "
    }, 
    {
      "type": "int", 
      "optional": "true", 
      "name": "n_restarts_optimizer", 
      "description": "The number of restarts of the optimizer for finding the kernel\\'s parameters which maximize the log-marginal likelihood. The first run of the optimizer is performed from the kernel\\'s initial parameters, the remaining ones (if any) from thetas sampled log-uniform randomly from the space of allowed theta-values. If greater than 0, all bounds must be finite. Note that n_restarts_optimizer == 0 implies that one run is performed. "
    }, 
    {
      "type": "boolean", 
      "optional": "true", 
      "name": "normalize_y", 
      "description": "Whether the target values y are normalized, i.e., the mean of the observed target values become zero. This parameter should be set to True if the target values\\' mean is expected to differ considerable from zero. When enabled, the normalization effectively modifies the GP\\'s prior based on the data, which contradicts the likelihood principle; normalization is thus disabled per default.  copy_X_train : bool, optional (default: True) If True, a persistent copy of the training data is stored in the object. Otherwise, just a reference to the training data is stored, which might cause predictions to change if the data is modified externally. "
    }, 
    {
      "type": "integer", 
      "optional": "true", 
      "name": "random_state", 
      "description": "The generator used to initialize the centers. If an integer is given, it fixes the seed. Defaults to the global numpy random number generator. "
    }
  ], 
  "tags": [
    "gaussian_process", 
    "gpr"
  ], 
  "common_name_unanalyzed": "Gaussian Process Regressor", 
  "schema_version": 1.0, 
  "languages": [
    "python2.7"
  ], 
  "version": "0.18.1", 
  "build": [
    {
      "type": "pip", 
      "package": "scikit-learn"
    }
  ], 
  "description": "'Gaussian process regression (GPR).\n\nThe implementation is based on Algorithm 2.1 of Gaussian Processes\nfor Machine Learning (GPML) by Rasmussen and Williams.\n\nIn addition to standard scikit-learn estimator API,\nGaussianProcessRegressor:\n\n* allows prediction without prior fitting (based on the GP prior)\n* provides an additional method sample_y(X), which evaluates samples\ndrawn from the GPR (prior or posterior) at given inputs\n* exposes a method log_marginal_likelihood(theta), which can be used\nexternally for other ways of selecting hyperparameters, e.g., via\nMarkov chain Monte Carlo.\n\nRead more in the :ref:`User Guide <gaussian_process>`.\n\n.. versionadded:: 0.18\n", 
  "methods_available": [
    {
      "id": "sklearn.gaussian_process.gpr.GaussianProcessRegressor.fit", 
      "returns": {
        "type": "returns", 
        "name": "self", 
        "description": "'"
      }, 
      "description": "'Fit Gaussian process regression model\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "Training data "
        }, 
        {
          "shape": "n_samples, [n_output_dims", 
          "type": "array-like", 
          "name": "y", 
          "description": "Target values "
        }
      ], 
      "name": "fit"
    }, 
    {
      "id": "sklearn.gaussian_process.gpr.GaussianProcessRegressor.get_params", 
      "returns": {
        "type": "mapping", 
        "name": "params", 
        "description": "Parameter names mapped to their values. '"
      }, 
      "description": "'Get parameters for this estimator.\n", 
      "parameters": [
        {
          "type": "boolean", 
          "optional": "true", 
          "name": "deep", 
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ], 
      "name": "get_params"
    }, 
    {
      "id": "sklearn.gaussian_process.gpr.GaussianProcessRegressor.log_marginal_likelihood", 
      "returns": {
        "type": "float", 
        "name": "log_likelihood", 
        "description": "Log-marginal likelihood of theta for training data.  log_likelihood_gradient : array, shape = (n_kernel_params,), optional Gradient of the log-marginal likelihood with respect to the kernel hyperparameters at position theta. Only returned when eval_gradient is True. '"
      }, 
      "description": "'Returns log-marginal likelihood of theta for training data.\n", 
      "parameters": [
        {
          "shape": "n_kernel_params,", 
          "type": "array-like", 
          "name": "theta", 
          "description": "Kernel hyperparameters for which the log-marginal likelihood is evaluated. If None, the precomputed log_marginal_likelihood of ``self.kernel_.theta`` is returned. "
        }, 
        {
          "type": "bool", 
          "name": "eval_gradient", 
          "description": "If True, the gradient of the log-marginal likelihood with respect to the kernel hyperparameters at position theta is returned additionally. If True, theta must not be None. "
        }
      ], 
      "name": "log_marginal_likelihood"
    }, 
    {
      "id": "sklearn.gaussian_process.gpr.GaussianProcessRegressor.predict", 
      "returns": {
        "shape": "n_samples, [n_output_dims", 
        "type": "array", 
        "name": "y_mean", 
        "description": "Mean of predictive distribution a query points  y_std : array, shape = (n_samples,), optional Standard deviation of predictive distribution at query points. Only returned when return_std is True.  y_cov : array, shape = (n_samples, n_samples), optional Covariance of joint predictive distribution a query points. Only returned when return_cov is True. '"
      }, 
      "description": "'Predict using the Gaussian process regression model\n\nWe can also predict based on an unfitted model by using the GP prior.\nIn addition to the mean of the predictive distribution, also its\nstandard deviation (return_std=True) or covariance (return_cov=True).\nNote that at most one of the two can be requested.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "Query points where the GP is evaluated "
        }, 
        {
          "type": "bool", 
          "name": "return_std", 
          "description": "If True, the standard-deviation of the predictive distribution at the query points is returned along with the mean. "
        }, 
        {
          "type": "bool", 
          "name": "return_cov", 
          "description": "If True, the covariance of the joint predictive distribution at the query points is returned along with the mean "
        }
      ], 
      "name": "predict"
    }, 
    {
      "id": "sklearn.gaussian_process.gpr.GaussianProcessRegressor.sample_y", 
      "returns": {
        "shape": "n_samples_X, [n_output_dims", 
        "type": "array", 
        "name": "y_samples", 
        "description": "Values of n_samples samples drawn from Gaussian process and evaluated at query points. '"
      }, 
      "description": "'Draw samples from Gaussian process and evaluate at X.\n", 
      "parameters": [
        {
          "shape": "n_samples_X, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "Query points where the GP samples are evaluated "
        }, 
        {
          "type": "int", 
          "name": "n_samples", 
          "description": "The number of samples drawn from the Gaussian process  random_state: RandomState or an int seed (0 by default) A random number generator instance "
        }
      ], 
      "name": "sample_y"
    }, 
    {
      "id": "sklearn.gaussian_process.gpr.GaussianProcessRegressor.score", 
      "returns": {
        "type": "float", 
        "name": "score", 
        "description": "R^2 of self.predict(X) wrt. y. '"
      }, 
      "description": "'Returns the coefficient of determination R^2 of the prediction.\n\nThe coefficient R^2 is defined as (1 - u/v), where u is the regression\nsum of squares ((y_true - y_pred) ** 2).sum() and v is the residual\nsum of squares ((y_true - y_true.mean()) ** 2).sum().\nBest possible score is 1.0 and it can be negative (because the\nmodel can be arbitrarily worse). A constant model that always\npredicts the expected value of y, disregarding the input features,\nwould get a R^2 score of 0.0.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "Test samples. "
        }, 
        {
          "shape": "n_samples", 
          "type": "array-like", 
          "name": "y", 
          "description": "True values for X. "
        }, 
        {
          "type": "array-like", 
          "shape": "n_samples", 
          "optional": "true", 
          "name": "sample_weight", 
          "description": "Sample weights. "
        }
      ], 
      "name": "score"
    }, 
    {
      "id": "sklearn.gaussian_process.gpr.GaussianProcessRegressor.set_params", 
      "returns": {
        "name": "self", 
        "description": "\""
      }, 
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n", 
      "parameters": [], 
      "name": "set_params"
    }
  ], 
  "learning_type": [
    "supervised"
  ], 
  "task_type": [
    "modeling"
  ], 
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/gaussian_process/gpr.py#L20", 
  "category_unanalyzed": "gaussian_process.gpr", 
  "name": "sklearn.gaussian_process.gpr.GaussianProcessRegressor", 
  "team": "jpl", 
  "attributes": [
    {
      "shape": "n_samples, n_features", 
      "type": "array-like", 
      "name": "X_train_", 
      "description": "Feature values in training data (also required for prediction) "
    }, 
    {
      "shape": "n_samples, [n_output_dims", 
      "type": "array-like", 
      "name": "y_train_", 
      "description": "Target values in training data (also required for prediction) "
    }, 
    {
      "type": "kernel", 
      "name": "kernel_", 
      "description": "The kernel used for prediction. The structure of the kernel is the same as the one passed as parameter but with optimized hyperparameters  L_ : array-like, shape = (n_samples, n_samples) Lower-triangular Cholesky decomposition of the kernel in ``X_train_`` "
    }, 
    {
      "shape": "n_samples,", 
      "type": "array-like", 
      "name": "alpha_", 
      "description": "Dual coefficients of training data points in kernel space "
    }, 
    {
      "type": "float", 
      "name": "log_marginal_likelihood_value_", 
      "description": "The log-marginal-likelihood of ``self.kernel_.theta`` "
    }
  ]
}

{
  "name": "sklearn.feature_selection.rfe.RFE",
  "id": "d3ced4d9dc982425bb6c42f74665c59e",
  "common_name": "RFE",
  "is_class": true,
  "tags": [
    "feature_selection",
    "rfe"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/feature_selection/rfe.py#L35",
  "parameters": [
    {
      "type": "object",
      "name": "estimator",
      "description": "A supervised learning estimator with a ``fit`` method that provides information about feature importance either through a ``coef_`` attribute or through a ``feature_importances_`` attribute. "
    },
    {
      "type": "int",
      "name": "n_features_to_select",
      "description": "The number of features to select. If `None`, half of the features are selected. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "1",
      "name": "step",
      "description": "If greater than or equal to 1, then `step` corresponds to the (integer) number of features to remove at each iteration. If within (0.0, 1.0), then `step` corresponds to the percentage (rounded down) of features to remove at each iteration. "
    },
    {
      "type": "int",
      "name": "verbose",
      "description": "Controls verbosity of output. "
    }
  ],
  "attributes": [
    {
      "type": "int",
      "name": "n_features_",
      "description": "The number of selected features. "
    },
    {
      "type": "array",
      "shape": "n_features",
      "name": "support_",
      "description": "The mask of selected features. "
    },
    {
      "type": "array",
      "shape": "n_features",
      "name": "ranking_",
      "description": "The feature ranking, such that ``ranking_[i]`` corresponds to the ranking position of the i-th feature. Selected (i.e., estimated best) features are assigned rank 1. "
    },
    {
      "type": "object",
      "name": "estimator_",
      "description": "The external estimator fit on the reduced dataset. "
    }
  ],
  "description": "'Feature ranking with recursive feature elimination.\n\nGiven an external estimator that assigns weights to features (e.g., the\ncoefficients of a linear model), the goal of recursive feature elimination\n(RFE) is to select features by recursively considering smaller and smaller\nsets of features. First, the estimator is trained on the initial set of\nfeatures and the importance of each feature is obtained either through a\n``coef_`` attribute or through a ``feature_importances_`` attribute.\nThen, the least important features are pruned from current set of features.\nThat procedure is recursively repeated on the pruned set until the desired\nnumber of features to select is eventually reached.\n\nRead more in the :ref:`User Guide <rfe>`.\n",
  "methods_available": [
    {
      "name": "decision_function",
      "id": "sklearn.feature_selection.rfe.RFE.decision_function",
      "parameters": [],
      "description": "None"
    },
    {
      "name": "fit",
      "id": "sklearn.feature_selection.rfe.RFE.fit",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The training input samples. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "The target values. '"
        }
      ],
      "description": "'Fit the RFE model and then the underlying estimator on the selected\nfeatures.\n"
    },
    {
      "name": "fit_transform",
      "id": "sklearn.feature_selection.rfe.RFE.fit_transform",
      "parameters": [
        {
          "type": "numpy",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training set. "
        },
        {
          "type": "numpy",
          "shape": "n_samples",
          "name": "y",
          "description": "Target values. "
        }
      ],
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n",
      "returns": {
        "type": "numpy",
        "shape": "n_samples, n_features_new",
        "name": "X_new",
        "description": "Transformed array.  '"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.feature_selection.rfe.RFE.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "get_support",
      "id": "sklearn.feature_selection.rfe.RFE.get_support",
      "parameters": [
        {
          "type": "boolean",
          "name": "indices",
          "description": "If True, the return value will be an array of integers, rather than a boolean mask. "
        }
      ],
      "description": "'\nGet a mask, or integer index, of the features selected\n",
      "returns": {
        "type": "array",
        "name": "support",
        "description": "An index that selects the retained features from a feature vector. If `indices` is False, this is a boolean array of shape [# input features], in which an element is True iff its corresponding feature is selected for retention. If `indices` is True, this is an integer array of shape [# output features] whose values are indices into the input feature vector. '"
      }
    },
    {
      "name": "inverse_transform",
      "id": "sklearn.feature_selection.rfe.RFE.inverse_transform",
      "parameters": [
        {
          "type": "array",
          "shape": "n_samples, n_selected_features",
          "name": "X",
          "description": "The input samples. "
        }
      ],
      "description": "'\nReverse the transformation operation\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_original_features",
        "name": "X_r",
        "description": "`X` with columns of zeros inserted where features would have been removed by `transform`. '"
      }
    },
    {
      "name": "predict",
      "id": "sklearn.feature_selection.rfe.RFE.predict",
      "parameters": [
        {
          "type": "array",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. "
        }
      ],
      "description": "'Reduce X to the selected features and then predict using the\nunderlying estimator.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples",
        "name": "y",
        "description": "The predicted target values. '"
      }
    },
    {
      "name": "predict_log_proba",
      "id": "sklearn.feature_selection.rfe.RFE.predict_log_proba",
      "parameters": [],
      "description": "None"
    },
    {
      "name": "predict_proba",
      "id": "sklearn.feature_selection.rfe.RFE.predict_proba",
      "parameters": [],
      "description": "None"
    },
    {
      "name": "score",
      "id": "sklearn.feature_selection.rfe.RFE.score",
      "parameters": [
        {
          "type": "array",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. "
        },
        {
          "type": "array",
          "shape": "n_samples",
          "name": "y",
          "description": "The target values. '"
        }
      ],
      "description": "'Reduce X to the selected features and then return the score of the\nunderlying estimator.\n"
    },
    {
      "name": "set_params",
      "id": "sklearn.feature_selection.rfe.RFE.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    },
    {
      "name": "transform",
      "id": "sklearn.feature_selection.rfe.RFE.transform",
      "parameters": [
        {
          "type": "array",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. "
        }
      ],
      "description": "'Reduce X to the selected features.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_selected_features",
        "name": "X_r",
        "description": "The input samples with only the selected features. '"
      }
    }
  ]
}
{
  "is_class": true,
  "library": "sklearn",
  "compute_resources": {},
  "common_name": "PCA",
  "id": "c80404e4-7b78-37f9-86a4-56d90c38d53b",
  "category": "decomposition.pca",
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/decomposition/pca.py#L106",
  "parameters": [
    {
      "type": "int",
      "name": "n_components",
      "description": "Number of components to keep. if n_components is not set all components are kept::  n_components == min(n_samples, n_features)  if n_components == \\'mle\\' and svd_solver == \\'full\\', Minka\\'s MLE is used to guess the dimension if ``0 < n_components < 1`` and svd_solver == \\'full\\', select the number of components such that the amount of variance that needs to be explained is greater than the percentage specified by n_components n_components cannot be equal to n_features for svd_solver == \\'arpack\\'. "
    },
    {
      "type": "bool",
      "optional": "true",
      "name": "whiten",
      "description": "When True (False by default) the `components_` vectors are multiplied by the square root of n_samples and then divided by the singular values to ensure uncorrelated outputs with unit component-wise variances.  Whitening will remove some information from the transformed signal (the relative variance scales of the components) but can sometime improve the predictive accuracy of the downstream estimators by making their data respect some hard-wired assumptions. "
    },
    {
      "type": "string",
      "name": "svd_solver",
      "description": "auto : the solver is selected by a default policy based on `X.shape` and `n_components`: if the input data is larger than 500x500 and the number of components to extract is lower than 80% of the smallest dimension of the data, then the more efficient \\'randomized\\' method is enabled. Otherwise the exact full SVD is computed and optionally truncated afterwards. full : run exact full SVD calling the standard LAPACK solver via `scipy.linalg.svd` and select the components by postprocessing arpack : run SVD truncated to n_components calling ARPACK solver via `scipy.sparse.linalg.svds`. It requires strictly 0 < n_components < X.shape[1] randomized : run randomized SVD by the method of Halko et al.  .. versionadded:: 0.18.0 "
    },
    {
      "type": "float",
      "optional": "true",
      "name": "tol",
      "description": "Tolerance for singular values computed by svd_solver == \\'arpack\\'.  .. versionadded:: 0.18.0 "
    },
    {
      "type": "int",
      "name": "iterated_power",
      "description": "Number of iterations for the power method computed by svd_solver == \\'randomized\\'.  .. versionadded:: 0.18.0 "
    },
    {
      "type": "int",
      "name": "random_state",
      "description": "Pseudo Random Number generator seed control. If None, use the numpy.random singleton. Used by svd_solver == \\'arpack\\' or \\'randomized\\'.  .. versionadded:: 0.18.0 "
    }
  ],
  "tags": [
    "decomposition",
    "pca"
  ],
  "common_name_unanalyzed": "PCA",
  "schema_version": 1,
  "languages": [
    "python2.7"
  ],
  "version": "0.18.1",
  "build": [
    {
      "type": "pip",
      "package": "scikit-learn"
    }
  ],
  "description": "'Principal component analysis (PCA)\n\nLinear dimensionality reduction using Singular Value Decomposition of the\ndata to project it to a lower dimensional space.\n\nIt uses the LAPACK implementation of the full SVD or a randomized truncated\nSVD by the method of Halko et al. 2009, depending on the shape of the input\ndata and the number of components to extract.\n\nIt can also use the scipy.sparse.linalg ARPACK implementation of the\ntruncated SVD.\n\nNotice that this class does not support sparse input. See\n:class:`TruncatedSVD` for an alternative with sparse data.\n\nRead more in the :ref:`User Guide <PCA>`.\n",
  "methods_available": [
    {
      "id": "sklearn.decomposition.pca.PCA.fit",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "Returns the instance itself. '"
      },
      "description": "'Fit the model with X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Training data, where n_samples in the number of samples and n_features is the number of features. "
        }
      ],
      "name": "fit"
    },
    {
      "id": "sklearn.decomposition.pca.PCA.fit_transform",
      "returns": {
        "shape": "n_samples, n_components",
        "type": "array-like",
        "name": "X_new",
        "description": " '"
      },
      "description": "'Fit the model with X and apply the dimensionality reduction on X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Training data, where n_samples is the number of samples and n_features is the number of features. "
        }
      ],
      "name": "fit_transform"
    },
    {
      "id": "sklearn.decomposition.pca.PCA.get_covariance",
      "returns": {
        "shape": "n_features, n_features",
        "type": "array",
        "name": "cov",
        "description": "Estimated covariance of data. '"
      },
      "description": "'Compute data covariance with the generative model.\n\n``cov = components_.T * S**2 * components_ + sigma2 * eye(n_features)``\nwhere  S**2 contains the explained variances, and sigma2 contains the\nnoise variances.\n",
      "parameters": [],
      "name": "get_covariance"
    },
    {
      "id": "sklearn.decomposition.pca.PCA.get_params",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      },
      "description": "'Get parameters for this estimator.\n",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "name": "get_params"
    },
    {
      "id": "sklearn.decomposition.pca.PCA.get_precision",
      "returns": {
        "shape": "n_features, n_features",
        "type": "array",
        "name": "precision",
        "description": "Estimated precision of data. '"
      },
      "description": "'Compute data precision matrix with the generative model.\n\nEquals the inverse of the covariance but computed with\nthe matrix inversion lemma for efficiency.\n",
      "parameters": [],
      "name": "get_precision"
    },
    {
      "id": "sklearn.decomposition.pca.PCA.inverse_transform",
      "returns": {
        "name": "X_original array-like, shape (n_samples, n_features)",
        "description": " Notes ----- If whitening is enabled, inverse_transform will compute the exact inverse operation, which includes reversing whitening. '"
      },
      "description": "'Transform data back to its original space.\n\nIn other words, return an input X_original whose transform would be X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_components",
          "type": "array-like",
          "name": "X",
          "description": "New data, where n_samples is the number of samples and n_components is the number of components. "
        }
      ],
      "name": "inverse_transform"
    },
    {
      "id": "sklearn.decomposition.pca.PCA.score",
      "returns": {
        "name": "ll: float",
        "description": "Average log-likelihood of the samples under the current model '"
      },
      "description": "'Return the average log-likelihood of all samples.\n\nSee. \"Pattern Recognition and Machine Learning\"\nby C. Bishop, 12.2.1 p. 574\nor http://www.miketipping.com/papers/met-mppca.pdf\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array",
          "name": "X",
          "description": "The data. "
        }
      ],
      "name": "score"
    },
    {
      "id": "sklearn.decomposition.pca.PCA.score_samples",
      "returns": {
        "name": "ll: array, shape (n_samples,)",
        "description": "Log-likelihood of each sample under the current model '"
      },
      "description": "'Return the log-likelihood of each sample.\n\nSee. \"Pattern Recognition and Machine Learning\"\nby C. Bishop, 12.2.1 p. 574\nor http://www.miketipping.com/papers/met-mppca.pdf\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array",
          "name": "X",
          "description": "The data. "
        }
      ],
      "name": "score_samples"
    },
    {
      "id": "sklearn.decomposition.pca.PCA.set_params",
      "returns": {
        "name": "self",
        "description": "\""
      },
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "parameters": [],
      "name": "set_params"
    },
    {
      "id": "sklearn.decomposition.pca.PCA.transform",
      "returns": {
        "shape": "n_samples, n_components",
        "type": "array-like",
        "name": "X_new",
        "description": " Examples --------  >>> import numpy as np >>> from sklearn.decomposition import IncrementalPCA >>> X = np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]]) >>> ipca = IncrementalPCA(n_components=2, batch_size=3) >>> ipca.fit(X) IncrementalPCA(batch_size=3, copy=True, n_components=2, whiten=False) >>> ipca.transform(X) # doctest: +SKIP '"
      },
      "description": "'Apply dimensionality reduction to X.\n\nX is projected on the first principal components previously extracted\nfrom a training set.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "New data, where n_samples is the number of samples and n_features is the number of features. "
        }
      ],
      "name": "transform"
    }
  ],
  "learning_type": [
    "supervised"
  ],
  "task_type": [
    "modeling"
  ],
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/decomposition/pca.py#L106",
  "category_unanalyzed": "decomposition.pca",
  "name": "sklearn.decomposition.pca.PCA",
  "team": "jpl",
  "attributes": [
    {
      "type": "array",
      "name": "components_",
      "description": "Principal axes in feature space, representing the directions of maximum variance in the data. The components are sorted by ``explained_variance_``. "
    },
    {
      "type": "array",
      "name": "explained_variance_",
      "description": "The amount of variance explained by each of the selected components.  .. versionadded:: 0.18 "
    },
    {
      "type": "array",
      "name": "explained_variance_ratio_",
      "description": "Percentage of variance explained by each of the selected components.  If ``n_components`` is not set then all components are stored and the sum of explained variances is equal to 1.0. "
    },
    {
      "type": "array",
      "name": "mean_",
      "description": "Per-feature empirical mean, estimated from the training set.  Equal to `X.mean(axis=1)`. "
    },
    {
      "type": "int",
      "name": "n_components_",
      "description": "The estimated number of components. When n_components is set to \\'mle\\' or a number between 0 and 1 (with svd_solver == \\'full\\') this number is estimated from input data. Otherwise it equals the parameter n_components, or n_features if n_components is None. "
    },
    {
      "type": "float",
      "name": "noise_variance_",
      "description": "The estimated noise covariance following the Probabilistic PCA model from Tipping and Bishop 1999. See \"Pattern Recognition and Machine Learning\" by C. Bishop, 12.2.1 p. 574 or http://www.miketipping.com/papers/met-mppca.pdf. It is required to computed the estimated data covariance and score samples.  References ---------- For n_components == \\'mle\\', this class uses the method of `Thomas P. Minka: Automatic Choice of Dimensionality for PCA. NIPS 2000: 598-604`  Implements the probabilistic PCA model from: M. Tipping and C. Bishop, Probabilistic Principal Component Analysis, Journal of the Royal Statistical Society, Series B, 61, Part 3, pp. 611-622 via the score and score_samples methods. See http://www.miketipping.com/papers/met-mppca.pdf  For svd_solver == \\'arpack\\', refer to `scipy.sparse.linalg.svds`.  For svd_solver == \\'randomized\\', see: `Finding structure with randomness: Stochastic algorithms for constructing approximate matrix decompositions Halko, et al., 2009 (arXiv:909)` `A randomized algorithm for the decomposition of matrices Per-Gunnar Martinsson, Vladimir Rokhlin and Mark Tygert`  "
    }
  ]
}

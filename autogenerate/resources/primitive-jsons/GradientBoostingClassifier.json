{
  "name": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier",
  "id": "3fd234b6de0b6f082c12cef5305d379b",
  "common_name": "GradientBoostingClassifier",
  "is_class": true,
  "tags": [
    "ensemble",
    "gradient_boosting"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/ensemble/gradient_boosting.py#L1225",
  "parameters": [
    {
      "type": "\\'deviance\\', \\'exponential\\'",
      "optional": "true",
      "default": "\\'deviance\\'",
      "name": "loss",
      "description": "loss function to be optimized. \\'deviance\\' refers to deviance (= logistic regression) for classification with probabilistic outputs. For loss \\'exponential\\' gradient boosting recovers the AdaBoost algorithm. "
    },
    {
      "type": "float",
      "optional": "true",
      "default": "0.1",
      "name": "learning_rate",
      "description": "learning rate shrinks the contribution of each tree by `learning_rate`. There is a trade-off between learning_rate and n_estimators. "
    },
    {
      "type": "int",
      "name": "n_estimators",
      "description": "The number of boosting stages to perform. Gradient boosting is fairly robust to over-fitting so a large number usually results in better performance. "
    },
    {
      "type": "integer",
      "optional": "true",
      "default": "3",
      "name": "max_depth",
      "description": "maximum depth of the individual regression estimators. The maximum depth limits the number of nodes in the tree. Tune this parameter for best performance; the best value depends on the interaction of the input variables. "
    },
    {
      "type": "string",
      "optional": "true",
      "default": "\"friedman_mse\"",
      "name": "criterion",
      "description": "The function to measure the quality of a split. Supported criteria are \"friedman_mse\" for the mean squared error with improvement score by Friedman, \"mse\" for mean squared error, and \"mae\" for the mean absolute error. The default value of \"friedman_mse\" is generally the best as it can provide a better approximation in some cases.  .. versionadded:: 0.18 "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "2",
      "name": "min_samples_split",
      "description": "The minimum number of samples required to split an internal node:  - If int, then consider `min_samples_split` as the minimum number. - If float, then `min_samples_split` is a percentage and `ceil(min_samples_split * n_samples)` are the minimum number of samples for each split.  .. versionchanged:: 0.18 Added float values for percentages. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "1",
      "name": "min_samples_leaf",
      "description": "The minimum number of samples required to be at a leaf node:  - If int, then consider `min_samples_leaf` as the minimum number. - If float, then `min_samples_leaf` is a percentage and `ceil(min_samples_leaf * n_samples)` are the minimum number of samples for each node.  .. versionchanged:: 0.18 Added float values for percentages. "
    },
    {
      "type": "float",
      "optional": "true",
      "default": "0.",
      "name": "min_weight_fraction_leaf",
      "description": "The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node. Samples have equal weight when sample_weight is not provided. "
    },
    {
      "type": "float",
      "optional": "true",
      "default": "1.0",
      "name": "subsample",
      "description": "The fraction of samples to be used for fitting the individual base learners. If smaller than 1.0 this results in Stochastic Gradient Boosting. `subsample` interacts with the parameter `n_estimators`. Choosing `subsample < 1.0` leads to a reduction of variance and an increase in bias. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "None",
      "name": "max_features",
      "description": "The number of features to consider when looking for the best split:  - If int, then consider `max_features` features at each split. - If float, then `max_features` is a percentage and `int(max_features * n_features)` features are considered at each split. - If \"auto\", then `max_features=sqrt(n_features)`. - If \"sqrt\", then `max_features=sqrt(n_features)`. - If \"log2\", then `max_features=log2(n_features)`. - If None, then `max_features=n_features`.  Choosing `max_features < n_features` leads to a reduction of variance and an increase in bias.  Note: the search for a split does not stop until at least one valid partition of the node samples is found, even if it requires to effectively inspect more than ``max_features`` features. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "None",
      "name": "max_leaf_nodes",
      "description": "Grow trees with ``max_leaf_nodes`` in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes. "
    },
    {
      "type": "float",
      "name": "min_impurity_split",
      "description": "Threshold for early stopping in tree growth. A node will split if its impurity is above the threshold, otherwise it is a leaf.  .. deprecated:: 0.19 ``min_impurity_split`` has been deprecated in favor of ``min_impurity_decrease`` in 0.19 and will be removed in 0.21. Use ``min_impurity_decrease`` instead. "
    },
    {
      "type": "float",
      "optional": "true",
      "default": "0.",
      "name": "min_impurity_decrease",
      "description": "A node will be split if this split induces a decrease of the impurity greater than or equal to this value.  The weighted impurity decrease equation is the following::  N_t / N * (impurity - N_t_R / N_t * right_impurity - N_t_L / N_t * left_impurity)  where ``N`` is the total number of samples, ``N_t`` is the number of samples at the current node, ``N_t_L`` is the number of samples in the left child, and ``N_t_R`` is the number of samples in the right child.  ``N``, ``N_t``, ``N_t_R`` and ``N_t_L`` all refer to the weighted sum, if ``sample_weight`` is passed.  .. versionadded:: 0.19 "
    },
    {
      "type": "",
      "optional": "true",
      "default": "None",
      "name": "init",
      "description": "An estimator object that is used to compute the initial predictions. ``init`` has to provide ``fit`` and ``predict``. If None it uses ``loss.init_estimator``. "
    },
    {
      "type": "int",
      "name": "verbose",
      "description": "Enable verbose output. If 1 then it prints progress and performance once in a while (the more trees the lower the frequency). If greater than 1 then it prints progress and performance for every tree. "
    },
    {
      "type": "bool",
      "name": "warm_start",
      "description": "When set to ``True``, reuse the solution of the previous call to fit and add more estimators to the ensemble, otherwise, just erase the previous solution. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "None",
      "name": "random_state",
      "description": "If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    },
    {
      "type": "bool",
      "optional": "true",
      "default": "\\'auto\\'",
      "name": "presort",
      "description": "Whether to presort the data to speed up the finding of best splits in fitting. Auto mode by default will use presorting on dense data and default to normal sorting on sparse data. Setting presort to true on sparse data will raise an error.  .. versionadded:: 0.17 *presort* parameter. "
    },
    {
      "type": "float",
      "optional": "true",
      "name": "validation_fraction",
      "description": "The proportion of training data to set aside as validation set for early stopping. Must be between 0 and 1. Only used if ``n_iter_no_change`` is set to an integer."
    },
    {
      "type": "int",
      "name": "n_iter_no_change",
      "description": "``n_iter_no_change`` is used to decide if early stopping will be used to terminate training when validation score is not improving. By default it is set to None to disable early stopping. If set to a number, it will set aside ``validation_fraction`` size of the training data as validation and terminate training when validation score is not improving in all of the previous ``n_iter_no_change`` numbers of iterations."
    },
    {
      "type": "float",
      "optional": "true",
      "name": "tol",
      "description": "Tolerance for the early stopping. When the loss is not improving by at least tol for ``n_iter_no_change`` iterations (if set to a number), the training stops."
    }
  ],
  "attributes": [
    {
      "type": "array",
      "shape": "n_estimators",
      "name": "oob_improvement_",
      "description": "The improvement in loss (= deviance) on the out-of-bag samples relative to the previous iteration. ``oob_improvement_[0]`` is the improvement in loss of the first stage over the ``init`` estimator. "
    },
    {
      "type": "array",
      "shape": "n_estimators",
      "name": "train_score_",
      "description": "The i-th score ``train_score_[i]`` is the deviance (= loss) of the model at iteration ``i`` on the in-bag sample. If ``subsample == 1`` this is the deviance on the training data. "
    },
    {
      "type": "",
      "name": "loss_",
      "description": "The concrete ``LossFunction`` object. "
    },
    {
      "type": "",
      "name": "init_",
      "description": "The estimator that provides the initial predictions. Set via the ``init`` argument or ``loss.init_estimator``. "
    },
    {
      "type": "ndarray",
      "shape": "n_estimators, ``loss_.K``",
      "name": "estimators_",
      "description": "The collection of fitted sub-estimators. ``loss_.K`` is 1 for binary classification, otherwise n_classes. "
    }
  ],
  "description": "'Gradient Boosting for classification.\n\nGB builds an additive model in a\nforward stage-wise fashion; it allows for the optimization of\narbitrary differentiable loss functions. In each stage ``n_classes_``\nregression trees are fit on the negative gradient of the\nbinomial or multinomial deviance loss function. Binary classification\nis a special case where only a single regression tree is induced.\n\nRead more in the :ref:`User Guide <gradient_boosting>`.\n",
  "methods_available": [
    {
      "name": "apply",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.apply",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. Internally, its dtype will be converted to ``dtype=np.float32``. If a sparse matrix is provided, it will be converted to a sparse ``csr_matrix``. "
        }
      ],
      "description": "'Apply trees in the ensemble to X, return leaf indices.\n\n.. versionadded:: 0.17\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_estimators, n_classes",
        "name": "X_leaves",
        "description": "For each datapoint x in X and for each tree in the ensemble, return the index of the leaf x ends up in each estimator. In the case of binary classification n_classes is 1. '"
      }
    },
    {
      "name": "decision_function",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.decision_function",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``. "
        }
      ],
      "description": "'Compute the decision function of ``X``.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_classes",
        "name": "score",
        "description": "The decision function of the input samples. The order of the classes corresponds to that in the attribute `classes_`. Regression and binary classification produce an array of shape [n_samples]. '"
      }
    },
    {
      "name": "fit",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.fit",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training vectors, where n_samples is the number of samples and n_features is the number of features. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "Target values (integers in classification, real numbers in regression) For classification, labels must correspond to classes. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "sample_weight",
          "description": "Sample weights. If None, then samples are equally weighted. Splits that would create child nodes with net zero or negative weight are ignored while searching for a split in each node. In the case of classification, splits are also ignored if they would result in any single class carrying a negative weight in either child node. "
        },
        {
          "type": "callable",
          "optional": "true",
          "name": "monitor",
          "description": "The monitor is called after each iteration with the current iteration, a reference to the estimator and the local variables of ``_fit_stages`` as keyword arguments ``callable(i, self, locals())``. If the callable returns ``True`` the fitting procedure is stopped. The monitor can be used for various things such as computing held-out estimates, early stopping, model introspect, and snapshoting. "
        }
      ],
      "description": "'Fit the gradient boosting model.\n",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "Returns self. '"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.feature_importances_",
      "returns": {
        "name": "feature_importances",
        "description": "\""
      },
      "description": "\"The importance of a feature is computed as the (normalized) total reduction of the criterion brought by that feature. It is also known as the Gini importance.\n",
      "parameters": [],
      "name": "feature_importances_"
    },
    {
      "name": "predict",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.predict",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``. "
        }
      ],
      "description": "'Predict class for X.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples",
        "name": "y",
        "description": "The predicted values. '"
      }
    },
    {
      "name": "predict_log_proba",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.predict_log_proba",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``.  Raises ------ AttributeError If the ``loss`` does not support probabilities. "
        }
      ],
      "description": "'Predict class log-probabilities for X.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples",
        "name": "p",
        "description": "The class log-probabilities of the input samples. The order of the classes corresponds to that in the attribute `classes_`. '"
      }
    },
    {
      "name": "predict_proba",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.predict_proba",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``.  Raises ------ AttributeError If the ``loss`` does not support probabilities. "
        }
      ],
      "description": "'Predict class probabilities for X.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples",
        "name": "p",
        "description": "The class probabilities of the input samples. The order of the classes corresponds to that in the attribute `classes_`. '"
      }
    },
    {
      "name": "score",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.score",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    },
    {
      "name": "staged_decision_function",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.staged_decision_function",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``. "
        }
      ],
      "description": "'Compute decision function of ``X`` for each iteration.\n\nThis method allows monitoring (i.e. determine error on testing set)\nafter each stage.\n",
      "returns": {
        "type": "generator",
        "shape": "n_samples, k",
        "name": "score",
        "description": "The decision function of the input samples. The order of the classes corresponds to that in the attribute `classes_`. Regression and binary classification are special cases with ``k == 1``, otherwise ``k==n_classes``. '"
      }
    },
    {
      "name": "staged_predict",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.staged_predict",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``. "
        }
      ],
      "description": "'Predict class at each stage for X.\n\nThis method allows monitoring (i.e. determine error on testing set)\nafter each stage.\n",
      "returns": {
        "type": "generator",
        "shape": "n_samples",
        "name": "y",
        "description": "The predicted value of the input samples. '"
      }
    },
    {
      "name": "staged_predict_proba",
      "id": "sklearn.ensemble.gradient_boosting.GradientBoostingClassifier.staged_predict_proba",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``. "
        }
      ],
      "description": "'Predict class probabilities at each stage for X.\n\nThis method allows monitoring (i.e. determine error on testing set)\nafter each stage.\n",
      "returns": {
        "type": "generator",
        "shape": "n_samples",
        "name": "y",
        "description": "The predicted value of the input samples. '"
      }
    }
  ]
}
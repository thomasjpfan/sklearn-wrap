{
  "name": "sklearn.random_projection.GaussianRandomProjection",
  "id": "acdc92359b6b15f84c4400ea7e3fc36a",
  "common_name": "GaussianRandomProjection",
  "is_class": true,
  "tags": [
    "random_projection"
  ],
  "version": "0.20.3",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.20.3/sklearn/random_projection.py#L424",
  "parameters": [
    {
      "type": "int",
      "optional": "true",
      "name": "n_components",
      "description": "Dimensionality of the target projection space.  n_components can be automatically adjusted according to the number of samples in the dataset and the bound given by the Johnson-Lindenstrauss lemma. In that case the quality of the embedding is controlled by the ``eps`` parameter.  It should be noted that Johnson-Lindenstrauss lemma can yield very conservative estimated of the required number of components as it makes no assumption on the structure of the dataset. "
    },
    {
      "type": "strictly",
      "optional": "true",
      "default": "0.1",
      "name": "eps",
      "description": "Parameter to control the quality of the embedding according to the Johnson-Lindenstrauss lemma when n_components is set to \\'auto\\'.  Smaller values lead to better embedding and higher number of dimensions (n_components) in the target projection space. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "None",
      "name": "random_state",
      "description": "Control the pseudo random number generator used to generate the matrix at fit time.  If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    }
  ],
  "attributes": [
    {
      "type": "int",
      "name": "n_component_",
      "description": "Concrete number of components computed when n_components=\"auto\". "
    },
    {
      "type": "numpy",
      "shape": "n_components, n_features",
      "name": "components_",
      "description": "Random matrix used for the projection. "
    }
  ],
  "description": "'Reduce dimensionality through Gaussian random projection\n\nThe components of the random matrix are drawn from N(0, 1 / n_components).\n\nRead more in the :ref:`User Guide <gaussian_random_matrix>`.\n",
  "methods_available": [
    {
      "name": "fit",
      "id": "sklearn.random_projection.GaussianRandomProjection.fit",
      "parameters": [
        {
          "type": "numpy",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training set: only the shape is used to find optimal random matrix dimensions based on the theory referenced in the afore mentioned papers.  y Ignored "
        }
      ],
      "description": "'Generate a sparse random projection matrix\n",
      "returns": {
        "name": "self",
        "description": " '"
      }
    },
    {
      "name": "fit_transform",
      "id": "sklearn.random_projection.GaussianRandomProjection.fit_transform",
      "parameters": [
        {
          "type": "numpy",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training set. "
        },
        {
          "type": "numpy",
          "shape": "n_samples",
          "name": "y",
          "description": "Target values. "
        }
      ],
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n",
      "returns": {
        "type": "numpy",
        "shape": "n_samples, n_features_new",
        "name": "X_new",
        "description": "Transformed array.  '"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.random_projection.GaussianRandomProjection.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.random_projection.GaussianRandomProjection.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    },
    {
      "name": "transform",
      "id": "sklearn.random_projection.GaussianRandomProjection.transform",
      "parameters": [
        {
          "type": "numpy",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input data to project into a smaller dimensional space. "
        }
      ],
      "description": "'Project the data by using matrix product with the random matrix\n",
      "returns": {
        "type": "numpy",
        "shape": "n_samples, n_components",
        "name": "X_new",
        "description": "Projected array. '"
      }
    }
  ]
}
{
  "handles_regression": false,
  "is_class": true,
  "library": "sklearn",
  "compute_resources": {},
  "common_name": "Ada Boost Classifier",
  "id": "437da2ac-3c55-37a2-96e8-135e8e061182",
  "handles_classification": true,
  "category": "ensemble.weight_boosting",
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/ensemble/weight_boosting.py#L295",
  "parameters": [
    {
      "default": "DecisionTreeClassifier",
      "type": "object",
      "optional": "true",
      "name": "base_estimator",
      "description": "The base estimator from which the boosted ensemble is built. Support for sample weighting is required, as well as proper `classes_` and `n_classes_` attributes. "
    },
    {
      "default": "50",
      "type": "integer",
      "optional": "true",
      "name": "n_estimators",
      "description": "The maximum number of estimators at which boosting is terminated. In case of perfect fit, the learning procedure is stopped early. "
    },
    {
      "default": "1.",
      "type": "float",
      "optional": "true",
      "name": "learning_rate",
      "description": "Learning rate shrinks the contribution of each classifier by ``learning_rate``. There is a trade-off between ``learning_rate`` and ``n_estimators``. "
    },
    {
      "default": "\\'SAMME.R\\'",
      "type": "\\'SAMME\\', \\'SAMME.R\\'",
      "optional": "true",
      "name": "algorithm",
      "description": "If \\'SAMME.R\\' then use the SAMME.R real boosting algorithm. ``base_estimator`` must support calculation of class probabilities. If \\'SAMME\\' then use the SAMME discrete boosting algorithm. The SAMME.R algorithm typically converges faster than SAMME, achieving a lower test error with fewer boosting iterations. "
    },
    {
      "default": "None",
      "type": "int",
      "optional": "true",
      "name": "random_state",
      "description": "If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    }
  ],
  "input_type": [
    "DENSE",
    "SPARSE",
    "UNSIGNED_DATA"
  ],
  "methods_available": [
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.decision_function",
      "returns": {
        "shape": "n_samples, k",
        "type": "array",
        "name": "score",
        "description": "The decision function of the input samples. The order of outputs is the same of that of the `classes_` attribute. Binary classification is a special cases with ``k == 1``, otherwise ``k==n_classes``. For binary classification, values closer to -1 or 1 mean more like the first or second class in ``classes_``, respectively. '"
      },
      "description": "'Compute the decision function of ``X``.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "The training input samples. Sparse matrix can be CSC, CSR, COO, DOK, or LIL. DOK and LIL are converted to CSR. "
        }
      ],
      "name": "decision_function"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.fit",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "Returns self. '"
      },
      "description": "'Build a boosted classifier from the training set (X, y).\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "The training input samples. Sparse matrix can be CSC, CSR, COO, DOK, or LIL. DOK and LIL are converted to CSR. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "The target values (class labels). "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. If None, the sample weights are initialized to ``1 / n_samples``. "
        }
      ],
      "name": "fit"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.get_params",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      },
      "description": "'Get parameters for this estimator.\n",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "name": "get_params"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.predict",
      "returns": {
        "shape": "n_samples",
        "type": "array",
        "name": "y",
        "description": "The predicted classes. '"
      },
      "description": "'Predict classes for X.\n\nThe predicted class of an input sample is computed as the weighted mean\nprediction of the classifiers in the ensemble.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "The training input samples. Sparse matrix can be CSC, CSR, COO, DOK, or LIL. DOK and LIL are converted to CSR. "
        }
      ],
      "name": "predict"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.predict_log_proba",
      "returns": {
        "shape": "n_samples",
        "type": "array",
        "name": "p",
        "description": "The class probabilities of the input samples. The order of outputs is the same of that of the `classes_` attribute. '"
      },
      "description": "'Predict class log-probabilities for X.\n\nThe predicted class log-probabilities of an input sample is computed as\nthe weighted mean predicted class log-probabilities of the classifiers\nin the ensemble.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "The training input samples. Sparse matrix can be CSC, CSR, COO, DOK, or LIL. DOK and LIL are converted to CSR. "
        }
      ],
      "name": "predict_log_proba"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.predict_proba",
      "returns": {
        "shape": "n_samples",
        "type": "array",
        "name": "p",
        "description": "The class probabilities of the input samples. The order of outputs is the same of that of the `classes_` attribute. '"
      },
      "description": "'Predict class probabilities for X.\n\nThe predicted class probabilities of an input sample is computed as\nthe weighted mean predicted class probabilities of the classifiers\nin the ensemble.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "The training input samples. Sparse matrix can be CSC, CSR, COO, DOK, or LIL. DOK and LIL are converted to CSR. "
        }
      ],
      "name": "predict_proba"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.score",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      },
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "name": "score"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.set_params",
      "returns": {
        "name": "self",
        "description": "\""
      },
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "parameters": [],
      "name": "set_params"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.staged_decision_function",
      "returns": {
        "shape": "n_samples, k",
        "type": "generator",
        "name": "score",
        "description": "The decision function of the input samples. The order of outputs is the same of that of the `classes_` attribute. Binary classification is a special cases with ``k == 1``, otherwise ``k==n_classes``. For binary classification, values closer to -1 or 1 mean more like the first or second class in ``classes_``, respectively. '"
      },
      "description": "'Compute decision function of ``X`` for each boosting iteration.\n\nThis method allows monitoring (i.e. determine error on testing set)\nafter each boosting iteration.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "The training input samples. Sparse matrix can be CSC, CSR, COO, DOK, or LIL. DOK and LIL are converted to CSR. "
        }
      ],
      "name": "staged_decision_function"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.staged_predict",
      "returns": {
        "shape": "n_samples",
        "type": "generator",
        "name": "y",
        "description": "The predicted classes. '"
      },
      "description": "'Return staged predictions for X.\n\nThe predicted class of an input sample is computed as the weighted mean\nprediction of the classifiers in the ensemble.\n\nThis generator method yields the ensemble prediction after each\niteration of boosting and therefore allows monitoring, such as to\ndetermine the prediction on a test set after each boost.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The input samples. "
        }
      ],
      "name": "staged_predict"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.feature_importances_",
      "returns": {
        "name": "feature_importances",
        "description": "\""
      },
      "description": "\"The importance of a feature is computed as the (normalized) total reduction of the criterion brought by that feature. It is also known as the Gini importance.\n",
      "parameters": [],
      "name": "feature_importances_"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.staged_predict_proba",
      "returns": {
        "shape": "n_samples",
        "type": "generator",
        "name": "p",
        "description": "The class probabilities of the input samples. The order of outputs is the same of that of the `classes_` attribute. '"
      },
      "description": "'Predict class probabilities for X.\n\nThe predicted class probabilities of an input sample is computed as\nthe weighted mean predicted class probabilities of the classifiers\nin the ensemble.\n\nThis generator method yields the ensemble predicted class probabilities\nafter each iteration of boosting and therefore allows monitoring, such\nas to determine the predicted class probabilities on a test set after\neach boost.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "The training input samples. Sparse matrix can be CSC, CSR, COO, DOK, or LIL. DOK and LIL are converted to CSR. "
        }
      ],
      "name": "staged_predict_proba"
    },
    {
      "id": "sklearn.ensemble.weight_boosting.AdaBoostClassifier.staged_score",
      "returns": {
        "type": "float",
        "name": "z",
        "description": "'"
      },
      "description": "'Return staged scores for X, y.\n\nThis generator method yields the ensemble score after each iteration of\nboosting and therefore allows monitoring, such as to determine the\nscore on a test set after each boost.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "The training input samples. Sparse matrix can be CSC, CSR, COO, DOK, or LIL. DOK and LIL are converted to CSR. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "Labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "name": "staged_score"
    }
  ],
  "common_name_unanalyzed": "Ada Boost Classifier",
  "schema_version": 1.0,
  "languages": [
    "python2.7"
  ],
  "version": "0.18.1",
  "build": [
    {
      "type": "pip",
      "package": "scikit-learn"
    }
  ],
  "handles_multiclass": true,
  "description": "'An AdaBoost classifier.\n\nAn AdaBoost [1] classifier is a meta-estimator that begins by fitting a\nclassifier on the original dataset and then fits additional copies of the\nclassifier on the same dataset but where the weights of incorrectly\nclassified instances are adjusted such that subsequent classifiers focus\nmore on difficult cases.\n\nThis class implements the algorithm known as AdaBoost-SAMME [2].\n\nRead more in the :ref:`User Guide <adaboost>`.\n",
  "tags": [
    "weight_boosting",
    "ensemble",
    "classification"
  ],
  "algorithm_type": [
    "classification"
  ],
  "learning_type": [
    "supervised"
  ],
  "task_type": [
    "modeling"
  ],
  "output_type": [
    "PREDICTIONS"
  ],
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/ensemble/weight_boosting.py#L295",
  "category_unanalyzed": "ensemble.weight_boosting",
  "name": "sklearn.ensemble.weight_boosting.AdaBoostClassifier",
  "handles_multilabel": true,
  "is_deterministic": true,
  "team": "jpl",
  "attributes": [
    {
      "type": "list",
      "name": "estimators_",
      "description": "The collection of fitted sub-estimators. "
    },
    {
      "shape": "n_classes",
      "type": "array",
      "name": "classes_",
      "description": "The classes labels. "
    },
    {
      "type": "int",
      "name": "n_classes_",
      "description": "The number of classes. "
    },
    {
      "type": "array",
      "name": "estimator_weights_",
      "description": "Weights for each estimator in the boosted ensemble. "
    },
    {
      "type": "array",
      "name": "estimator_errors_",
      "description": "Classification error for each estimator in the boosted ensemble. "
    }
  ]
}

{
  "name": "sklearn.linear_model.coordinate_descent.Lasso",
  "id": "e9370edbe6e1f784f7092756ce0acfeb",
  "common_name": "Lasso",
  "is_class": true,
  "tags": [
    "linear_model",
    "coordinate_descent"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/linear_model/coordinate_descent.py#L797",
  "parameters": [
    {
      "type": "float",
      "optional": "true",
      "name": "alpha",
      "description": "Constant that multiplies the L1 term. Defaults to 1.0. ``alpha = 0`` is equivalent to an ordinary least square, solved by the :class:`LinearRegression` object. For numerical reasons, using ``alpha = 0`` with the ``Lasso`` object is not advised. Given this, you should use the :class:`LinearRegression` object. "
    },
    {
      "type": "boolean",
      "name": "fit_intercept",
      "description": "whether to calculate the intercept for this model. If set to false, no intercept will be used in calculations (e.g. data is expected to be already centered). "
    },
    {
      "type": "boolean",
      "optional": "true",
      "name": "normalize",
      "description": "This parameter is ignored when ``fit_intercept`` is set to False. If True, the regressors X will be normalized before regression by subtracting the mean and dividing by the l2-norm. If you wish to standardize, please use :class:`sklearn.preprocessing.StandardScaler` before calling ``fit`` on an estimator with ``normalize=False``. "
    },
    {
      "type": "",
      "name": "precompute",
      "description": "Whether to use a precomputed Gram matrix to speed up calculations. If set to ``'auto'`` let us decide. The Gram matrix can also be passed as argument. For sparse input this option is always ``True`` to preserve sparsity.  copy_X : boolean, optional, default True If ``True``, X will be copied; else, it may be overwritten. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "max_iter",
      "description": "The maximum number of iterations "
    },
    {
      "type": "float",
      "optional": "true",
      "name": "tol",
      "description": "The tolerance for the optimization: if the updates are smaller than ``tol``, the optimization code checks the dual gap for optimality and continues until it is smaller than ``tol``. "
    },
    {
      "type": "bool",
      "optional": "true",
      "name": "warm_start",
      "description": "When set to True, reuse the solution of the previous call to fit as initialization, otherwise, just erase the previous solution. "
    },
    {
      "type": "bool",
      "optional": "true",
      "name": "positive",
      "description": "When set to ``True``, forces the coefficients to be positive. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "random_state",
      "description": "The seed of the pseudo random number generator that selects a random feature to update.  If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. Used when ``selection`` == 'random'. "
    },
    {
      "type": "str",
      "name": "selection",
      "description": "If set to 'random', a random coefficient is updated every iteration rather than looping over features sequentially by default. This (setting to 'random') often leads to significantly faster convergence especially when tol is higher than 1e-4. "
    }
  ],
  "attributes": [
    {
      "type": "array",
      "shape": "n_features,",
      "name": "coef_",
      "description": "parameter vector (w in the cost function formula) "
    },
    {
      "type": "float",
      "shape": "n_targets,",
      "name": "intercept_",
      "description": "independent term in decision function. "
    },
    {
      "type": "int",
      "shape": "n_targets,",
      "name": "n_iter_",
      "description": "number of iterations run by the coordinate descent solver to reach the specified tolerance. "
    }
  ],
  "description": "\"Linear Model trained with L1 prior as regularizer (aka the Lasso)\n\nThe optimization objective for Lasso is::\n\n(1 / (2 * n_samples)) * ||y - Xw||^2_2 + alpha * ||w||_1\n\nTechnically the Lasso model is optimizing the same objective function as\nthe Elastic Net with ``l1_ratio=1.0`` (no L2 penalty).\n\nRead more in the :ref:`User Guide <lasso>`.\n",
  "methods_available": [
    {
      "name": "fit",
      "id": "sklearn.linear_model.coordinate_descent.Lasso.fit",
      "parameters": [
        {
          "type": "ndarray",
          "name": "X",
          "description": "Data "
        },
        {
          "type": "ndarray",
          "shape": "n_samples,",
          "name": "y",
          "description": "Target. Will be cast to X's dtype if necessary "
        },
        {
          "type": "boolean",
          "name": "check_input",
          "description": "Allow to bypass several input checking. Don't use this parameter unless you know what you do.  Notes -----  Coordinate descent is an algorithm that considers each column of data at a time hence it will automatically convert the X input as a Fortran-contiguous numpy array if necessary.  To avoid memory re-allocation it is advised to allocate the initial data in memory directly using that format. \""
        }
      ],
      "description": "\"Fit model with coordinate descent.\n"
    },
    {
      "name": "get_params",
      "id": "sklearn.linear_model.coordinate_descent.Lasso.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "enet_path",
      "id": "sklearn.linear_model.coordinate_descent.Lasso.enet_path",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training data. Pass directly as Fortran-contiguous data to avoid unnecessary memory duplication. If ``y`` is mono-output then ``X`` can be sparse. "
        },
        {
          "type": "ndarray",
          "shape": "n_samples,",
          "name": "y",
          "description": "Target values "
        },
        {
          "type": "float",
          "optional": "true",
          "name": "l1_ratio",
          "description": "float between 0 and 1 passed to elastic net (scaling between l1 and l2 penalties). ``l1_ratio=1`` corresponds to the Lasso "
        },
        {
          "type": "float",
          "name": "eps",
          "description": "Length of the path. ``eps=1e-3`` means that ``alpha_min / alpha_max = 1e-3`` "
        },
        {
          "type": "int",
          "optional": "true",
          "name": "n_alphas",
          "description": "Number of alphas along the regularization path "
        },
        {
          "type": "ndarray",
          "optional": "true",
          "name": "alphas",
          "description": "List of alphas where to compute the models. If None alphas are set automatically "
        },
        {
          "type": "",
          "name": "precompute",
          "description": "Whether to use a precomputed Gram matrix to speed up calculations. If set to ``'auto'`` let us decide. The Gram matrix can also be passed as argument.  Xy : array-like, optional Xy = np.dot(X.T, y) that can be precomputed. It is useful only when the Gram matrix is precomputed.  copy_X : boolean, optional, default True If ``True``, X will be copied; else, it may be overwritten. "
        },
        {
          "type": "array",
          "shape": "n_features, ",
          "name": "coef_init",
          "description": "The initial values of the coefficients. "
        },
        {
          "type": "bool",
          "name": "verbose",
          "description": "Amount of verbosity. "
        },
        {
          "type": "bool",
          "name": "return_n_iter",
          "description": "whether to return the number of iterations or not. "
        },
        {
          "type": "bool",
          "name": "positive",
          "description": "If set to True, forces coefficients to be positive. (Only allowed when ``y.ndim == 1``). "
        },
        {
          "type": "bool",
          "name": "check_input",
          "description": "Skip input validation checks, including the Gram matrix when provided assuming there are handled by the caller when check_input=False.  **params : kwargs keyword arguments passed to the coordinate descent solver. "
        }
      ],
      "description": "\"Compute elastic net path with coordinate descent\n\nThe elastic net optimization function varies for mono and multi-outputs.\n\nFor mono-output tasks it is::\n\n1 / (2 * n_samples) * ||y - Xw||^2_2\n+ alpha * l1_ratio * ||w||_1\n+ 0.5 * alpha * (1 - l1_ratio) * ||w||^2_2\n\nFor multi-output tasks it is::\n\n(1 / (2 * n_samples)) * ||Y - XW||^Fro_2\n+ alpha * l1_ratio * ||W||_21\n+ 0.5 * alpha * (1 - l1_ratio) * ||W||_Fro^2\n\nWhere::\n\n||W||_21 = \\\\sum_i \\\\sqrt{\\\\sum_j w_{ij}^2}\n\ni.e. the sum of norm of each row.\n\nRead more in the :ref:`User Guide <elastic_net>`.\n",
      "returns": {
        "type": "array",
        "shape": "n_alphas,",
        "name": "alphas",
        "description": "The alphas along the path where models are computed.  coefs : array, shape (n_features, n_alphas) or             (n_outputs, n_features, n_alphas) Coefficients along the path.  dual_gaps : array, shape (n_alphas,) The dual gaps at the end of the optimization for each alpha.  n_iters : array-like, shape (n_alphas,) The number of iterations taken by the coordinate descent optimizer to reach the specified tolerance for each alpha. (Is returned when ``return_n_iter`` is set to True).  Notes ----- For an example, see :ref:`examples/linear_model/plot_lasso_coordinate_descent_path.py <sphx_glr_auto_examples_linear_model_plot_lasso_coordinate_descent_path.py>`.  See also -------- MultiTaskElasticNet MultiTaskElasticNetCV ElasticNet ElasticNetCV \""
      }
    },
    {
      "name": "predict",
      "id": "sklearn.linear_model.coordinate_descent.Lasso.predict",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "description": "'Predict using the linear model\n",
      "returns": {
        "type": "array",
        "shape": "n_samples,",
        "name": "C",
        "description": "Returns predicted values. '"
      }
    },
    {
      "name": "score",
      "id": "sklearn.linear_model.coordinate_descent.Lasso.score",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "True values for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "description": "'Returns the coefficient of determination R^2 of the prediction.\n\nThe coefficient R^2 is defined as (1 - u/v), where u is the residual\nsum of squares ((y_true - y_pred) ** 2).sum() and v is the total\nsum of squares ((y_true - y_true.mean()) ** 2).sum().\nThe best possible score is 1.0 and it can be negative (because the\nmodel can be arbitrarily worse). A constant model that always\npredicts the expected value of y, disregarding the input features,\nwould get a R^2 score of 0.0.\n",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "R^2 of self.predict(X) wrt. y. '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.linear_model.coordinate_descent.Lasso.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    }
  ]
}
{
  "name": "sklearn.mixture.gaussian_mixture.GaussianMixture",
  "id": "22709914c7aca87306556e4554cdbf8a",
  "common_name": "GaussianMixture",
  "is_class": true,
  "tags": [
    "mixture",
    "gaussian_mixture"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/mixture/gaussian_mixture.py#L435",
  "parameters": [
    {
      "type": "int",
      "name": "n_components",
      "description": "The number of mixture components. "
    },
    {
      "type": "'full', 'tied', 'diag', 'spherical'",
      "name": "covariance_type",
      "description": "defaults to 'full'. String describing the type of covariance parameters to use. Must be one of::  'full' (each component has its own general covariance matrix), 'tied' (all components share the same general covariance matrix), 'diag' (each component has its own diagonal covariance matrix), 'spherical' (each component has its own single variance). "
    },
    {
      "type": "float",
      "name": "tol",
      "description": "The convergence threshold. EM iterations will stop when the lower bound average gain is below this threshold. "
    },
    {
      "type": "float",
      "name": "reg_covar",
      "description": "Non-negative regularization added to the diagonal of covariance. Allows to assure that the covariance matrices are all positive. "
    },
    {
      "type": "int",
      "name": "max_iter",
      "description": "The number of EM iterations to perform. "
    },
    {
      "type": "int",
      "name": "n_init",
      "description": "The number of initializations to perform. The best results are kept. "
    },
    {
      "type": "'kmeans', 'random'",
      "name": "init_params",
      "description": "The method used to initialize the weights, the means and the precisions. Must be one of::  'kmeans' : responsibilities are initialized using kmeans. 'random' : responsibilities are initialized randomly. "
    },
    {
      "type": "array-like",
      "shape": "n_components, ",
      "optional": "true",
      "name": "weights_init",
      "description": "The user-provided initial weights, defaults to None. If it None, weights are initialized using the `init_params` method. "
    },
    {
      "type": "array-like",
      "shape": "n_components, n_features",
      "optional": "true",
      "name": "means_init",
      "description": "The user-provided initial means, defaults to None, If it None, means are initialized using the `init_params` method. "
    },
    {
      "type": "array-like",
      "optional": "true",
      "name": "precisions_init",
      "description": "The user-provided initial precisions (inverse of the covariance matrices), defaults to None. If it None, precisions are initialized using the 'init_params' method. The shape depends on 'covariance_type'::  (n_components,)                        if 'spherical', (n_features, n_features)               if 'tied', (n_components, n_features)             if 'diag', (n_components, n_features, n_features) if 'full' "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "None",
      "name": "random_state",
      "description": "If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    },
    {
      "type": "bool",
      "name": "warm_start",
      "description": "If 'warm_start' is True, the solution of the last fitting is used as initialization for the next call of fit(). This can speed up convergence when fit is called several time on similar problems. "
    },
    {
      "type": "int",
      "name": "verbose",
      "description": "Enable verbose output. If 1 then it prints the current initialization and each iteration step. If greater than 1 then it prints also the log probability and the time needed for each step. "
    },
    {
      "type": "int",
      "name": "verbose_interval",
      "description": "Number of iteration done before the next print. "
    }
  ],
  "attributes": [
    {
      "type": "array-like",
      "shape": "n_components,",
      "name": "weights_",
      "description": "The weights of each mixture components. "
    },
    {
      "type": "array-like",
      "shape": "n_components, n_features",
      "name": "means_",
      "description": "The mean of each mixture component. "
    },
    {
      "type": "array-like",
      "name": "covariances_",
      "description": "The covariance of each mixture component. The shape depends on `covariance_type`::  (n_components,)                        if 'spherical', (n_features, n_features)               if 'tied', (n_components, n_features)             if 'diag', (n_components, n_features, n_features) if 'full' "
    },
    {
      "type": "array-like",
      "name": "precisions_",
      "description": "The precision matrices for each component in the mixture. A precision matrix is the inverse of a covariance matrix. A covariance matrix is symmetric positive definite so the mixture of Gaussian can be equivalently parameterized by the precision matrices. Storing the precision matrices instead of the covariance matrices makes it more efficient to compute the log-likelihood of new samples at test time. The shape depends on `covariance_type`::  (n_components,)                        if 'spherical', (n_features, n_features)               if 'tied', (n_components, n_features)             if 'diag', (n_components, n_features, n_features) if 'full' "
    },
    {
      "type": "array-like",
      "name": "precisions_cholesky_",
      "description": "The cholesky decomposition of the precision matrices of each mixture component. A precision matrix is the inverse of a covariance matrix. A covariance matrix is symmetric positive definite so the mixture of Gaussian can be equivalently parameterized by the precision matrices. Storing the precision matrices instead of the covariance matrices makes it more efficient to compute the log-likelihood of new samples at test time. The shape depends on `covariance_type`::  (n_components,)                        if 'spherical', (n_features, n_features)               if 'tied', (n_components, n_features)             if 'diag', (n_components, n_features, n_features) if 'full' "
    },
    {
      "type": "bool",
      "name": "converged_",
      "description": "True when convergence was reached in fit(), False otherwise. "
    },
    {
      "type": "int",
      "name": "n_iter_",
      "description": "Number of step used by the best fit of EM to reach the convergence. "
    },
    {
      "type": "float",
      "name": "lower_bound_",
      "description": "Log-likelihood of the best fit of EM.  See Also -------- BayesianGaussianMixture : Gaussian mixture model fit with a variational inference."
    }
  ],
  "description": "\"Gaussian Mixture.\n\nRepresentation of a Gaussian mixture model probability distribution.\nThis class allows to estimate the parameters of a Gaussian mixture\ndistribution.\n\nRead more in the :ref:`User Guide <gmm>`.\n\n.. versionadded:: 0.18\n",
  "methods_available": [
    {
      "name": "aic",
      "id": "sklearn.mixture.gaussian_mixture.GaussianMixture.aic",
      "parameters": [
        {
          "type": "array",
          "shape": "n_samples, n_dimensions",
          "name": "X",
          "description": ""
        }
      ],
      "description": "'Akaike information criterion for the current model on the input X.\n",
      "returns": {
        "type": "float",
        "name": "aic",
        "description": "The lower the better. '"
      }
    },
    {
      "name": "bic",
      "id": "sklearn.mixture.gaussian_mixture.GaussianMixture.bic",
      "parameters": [
        {
          "type": "array",
          "shape": "n_samples, n_dimensions",
          "name": "X",
          "description": ""
        }
      ],
      "description": "'Bayesian information criterion for the current model on the input X.\n",
      "returns": {
        "type": "float",
        "name": "bic",
        "description": "The lower the better. '"
      }
    },
    {
      "name": "fit",
      "id": "sklearn.mixture.gaussian_mixture.GaussianMixture.fit",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "List of n_features-dimensional data points. Each row corresponds to a single data point. "
        }
      ],
      "description": "'Estimate model parameters with the EM algorithm.\n\nThe method fit the model `n_init` times and set the parameters with\nwhich the model has the largest likelihood or lower bound. Within each\ntrial, the method iterates between E-step and M-step for `max_iter`\ntimes until the change of likelihood or lower bound is less than\n`tol`, otherwise, a `ConvergenceWarning` is raised.\n",
      "returns": {
        "name": "self",
        "description": "'"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.mixture.gaussian_mixture.GaussianMixture.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "predict",
      "id": "sklearn.mixture.gaussian_mixture.GaussianMixture.predict",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "List of n_features-dimensional data points. Each row corresponds to a single data point. "
        }
      ],
      "description": "'Predict the labels for the data samples in X using trained model.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples,",
        "name": "labels",
        "description": "Component labels. '"
      }
    },
    {
      "name": "predict_proba",
      "id": "sklearn.mixture.gaussian_mixture.GaussianMixture.predict_proba",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "List of n_features-dimensional data points. Each row corresponds to a single data point. "
        }
      ],
      "description": "'Predict posterior probability of each component given the data.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_components",
        "name": "resp",
        "description": "Returns the probability each Gaussian (state) in the model given each sample. '"
      }
    },
    {
      "name": "sample",
      "id": "sklearn.mixture.gaussian_mixture.GaussianMixture.sample",
      "parameters": [
        {
          "type": "int",
          "optional": "true",
          "name": "n_samples",
          "description": "Number of samples to generate. Defaults to 1. "
        }
      ],
      "description": "'Generate random samples from the fitted Gaussian distribution.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_features",
        "name": "X",
        "description": "Randomly generated sample  y : array, shape (nsamples,) Component labels  '"
      }
    },
    {
      "name": "score",
      "id": "sklearn.mixture.gaussian_mixture.GaussianMixture.score",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_dimensions",
          "name": "X",
          "description": "List of n_features-dimensional data points. Each row corresponds to a single data point. "
        }
      ],
      "description": "'Compute the per-sample average log-likelihood of the given data X.\n",
      "returns": {
        "type": "float",
        "name": "log_likelihood",
        "description": "Log likelihood of the Gaussian mixture given X. '"
      }
    },
    {
      "name": "score_samples",
      "id": "sklearn.mixture.gaussian_mixture.GaussianMixture.score_samples",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "List of n_features-dimensional data points. Each row corresponds to a single data point. "
        }
      ],
      "description": "'Compute the weighted log probabilities for each sample.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples,",
        "name": "log_prob",
        "description": "Log probabilities of each data point in X. '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.mixture.gaussian_mixture.GaussianMixture.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    }
  ]
}
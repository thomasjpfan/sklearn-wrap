{
  "name": "sklearn.linear_model.least_angle.Lars",
  "id": "0ce1fb3acfeb18f6ea45241ce0414d8b",
  "common_name": "Lars",
  "is_class": true,
  "tags": [
    "linear_model",
    "least_angle"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/linear_model/least_angle.py#L499",
  "parameters": [
    {
      "type": "boolean",
      "name": "fit_intercept",
      "description": "Whether to calculate the intercept for this model. If set to false, no intercept will be used in calculations (e.g. data is expected to be already centered). "
    },
    {
      "type": "boolean",
      "optional": "true",
      "name": "verbose",
      "description": "Sets the verbosity amount "
    },
    {
      "type": "boolean",
      "optional": "true",
      "name": "normalize",
      "description": "This parameter is ignored when ``fit_intercept`` is set to False. If True, the regressors X will be normalized before regression by subtracting the mean and dividing by the l2-norm. If you wish to standardize, please use :class:`sklearn.preprocessing.StandardScaler` before calling ``fit`` on an estimator with ``normalize=False``. "
    },
    {
      "type": "",
      "name": "precompute",
      "description": "Whether to use a precomputed Gram matrix to speed up calculations. If set to ``'auto'`` let us decide. The Gram matrix can also be passed as argument. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "n_nonzero_coefs",
      "description": "Target number of non-zero coefficients. Use ``np.inf`` for no limit. "
    },
    {
      "type": "float",
      "optional": "true",
      "name": "eps",
      "description": "The machine-precision regularization in the computation of the Cholesky diagonal factors. Increase this for very ill-conditioned systems. Unlike the ``tol`` parameter in some iterative optimization-based algorithms, this parameter does not control the tolerance of the optimization.  copy_X : boolean, optional, default True If ``True``, X will be copied; else, it may be overwritten. "
    },
    {
      "type": "boolean",
      "name": "fit_path",
      "description": "If True the full path is stored in the ``coef_path_`` attribute. If you compute the solution for a large problem or many targets, setting ``fit_path`` to ``False`` will lead to a speedup, especially with a small alpha. "
    },
    {
      "type": "boolean",
      "name": "positive",
      "description": "Restrict coefficients to be >= 0. Be aware that you might want to remove fit_intercept which is set True by default. "
    }
  ],
  "attributes": [
    {
      "type": "array",
      "shape": "n_alphas + 1,",
      "name": "alphas_",
      "description": "Maximum of covariances (in absolute value) at each iteration.         ``n_alphas`` is either ``n_nonzero_coefs`` or ``n_features``,         whichever is smaller. "
    },
    {
      "type": "list",
      "name": "active_",
      "description": "Indices of active variables at the end of the path. "
    },
    {
      "type": "array",
      "shape": "n_features, n_alphas + 1",
      "name": "coef_path_",
      "description": "The varying values of the coefficients along the path. It is not present if the ``fit_path`` parameter is ``False``. "
    },
    {
      "type": "array",
      "shape": "n_features,",
      "name": "coef_",
      "description": "Parameter vector (w in the formulation formula). "
    },
    {
      "type": "float",
      "shape": "n_targets,",
      "name": "intercept_",
      "description": "Independent term in decision function. "
    },
    {
      "type": "array-like",
      "name": "n_iter_",
      "description": "The number of iterations taken by lars_path to find the grid of alphas for each target. "
    }
  ],
  "description": "\"Least Angle Regression model a.k.a. LAR\n\nRead more in the :ref:`User Guide <least_angle_regression>`.\n",
  "methods_available": [
    {
      "name": "fit",
      "id": "sklearn.linear_model.least_angle.Lars.fit",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training data. "
        },
        {
          "type": "array-like",
          "shape": "n_samples,",
          "name": "y",
          "description": "Target values.  Xy : array-like, shape (n_samples,) or (n_samples, n_targets),                 optional Xy = np.dot(X.T, y) that can be precomputed. It is useful only when the Gram matrix is precomputed. "
        }
      ],
      "description": "'Fit the model using X, y as training data.\n",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "returns an instance of self. '"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.linear_model.least_angle.Lars.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "predict",
      "id": "sklearn.linear_model.least_angle.Lars.predict",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "description": "'Predict using the linear model\n",
      "returns": {
        "type": "array",
        "shape": "n_samples,",
        "name": "C",
        "description": "Returns predicted values. '"
      }
    },
    {
      "name": "score",
      "id": "sklearn.linear_model.least_angle.Lars.score",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "True values for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "description": "'Returns the coefficient of determination R^2 of the prediction.\n\nThe coefficient R^2 is defined as (1 - u/v), where u is the residual\nsum of squares ((y_true - y_pred) ** 2).sum() and v is the total\nsum of squares ((y_true - y_true.mean()) ** 2).sum().\nThe best possible score is 1.0 and it can be negative (because the\nmodel can be arbitrarily worse). A constant model that always\npredicts the expected value of y, disregarding the input features,\nwould get a R^2 score of 0.0.\n",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "R^2 of self.predict(X) wrt. y. '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.linear_model.least_angle.Lars.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    }
  ]
}
#### Understanding `Choice` hyper-parameter

Some parameters can be dependant on other parameters, for instance they are relevant only if some
other parameter has certain value. This can sometimes be resolved by creating `Choice` hyper-parameter. 

There are two ways template files are handling `Choice` hyper-parameter, deciding on whether `parameters_placeholder`
is defined in hyper-parameter in overlay or not.

##### 1) Passing dependant hyper-parameters in one attribute as dict

This is useful when sklearn's class has one parameter with finite number of choices and 
one other parameter, which is placeholder for all parameters that depend on it.

Example: hyper-parameter handling both `fun` and `fun_args` in 
[`FastICA`](https://github.com/scikit-learn/scikit-learn/blob/f0ab589f/sklearn/decomposition/fastica_.py#L398) 
is this: 
```
fun = hyperparams.Choice(
    choices={
        'logcosh': hyperparams.Hyperparams.define(
            configuration=OrderedDict({
                'alpha': hyperparams.Hyperparameter[float](
                    default=1,
                    semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
                )
            })
        ),
        ...
    }
    ...
)
```
When initializing sklearn class, this is written:
```
fun=self.hyperparams['fun']['choice'],
fun_args=self.hyperparams['fun'],
```

To achieve this, you write this in overlay file:

```
{
  "name": "fun",
  "type": "Choice",
  "parameters_placeholder": "fun_args",
  "hyperparams": {
    "alpha": {
      "name": "alpha",
      "type": "Hyperparameter",
      "init_args": {
        "default": 1.0,
        "_structural_type": "float"
      }
    }
  },
  "groups": {
    "logcosh": [
      "alpha"
    ],
    "exp": [],
    "cube": []
  },
  "init_args": {
    "default": "logcosh"
  }
}
```

##### 2) Passing dependant hyper-parameters as separate attributes
This is useful when sklearn's class has one parameter with finite number of choices and
other parameters, which are relevant only at some configurations of it. 

Example: hyper-parameter handling `kernel`, `degree`, `gamma`, and `coef0` in 
[`SVR`](https://github.com/scikit-learn/scikit-learn/blob/f0ab589f/sklearn/svm/classes.py#L761) 
is similar to hyper-parameter in previous example, but when initializing sklearn class, this is written:
```
kernel=self.hyperparams['kernel']['choice'],
degree=self.hyperparams['kernel'].get('degree', 3),
gamma=self.hyperparams['kernel'].get('gamma', 'auto'),
coef0=self.hyperparams['kernel'].get('coef0', 0),
```

To achieve this, you write this in overlay file:

```json
{
  "name": "kernel",
  "type": "Choice",
  "hyperparams": {
    "degree": {
      "type": "Bounded",
      "name": "degree",
      "init_args": {
        "default": 3,
        "lower": 0,
        "upper": "None",
        "_structural_type": "int"
      }
    },
    ...
  },
  "groups": {
    "poly": [
      "degree",
      "gamma",
      "coef0"
    ],
    ...
  },
  "init_args": {
    "default": "rbf"
  }
}
```




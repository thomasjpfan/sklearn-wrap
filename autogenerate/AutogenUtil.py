from jinja2 import Template, Environment, PackageLoader


class TemplateUtil:

    def __init__(self, env: Environment = None, template: Template = None):
        self.env = env
        self.template = template

    def load_env(self, template_dir="templates", app_name='autogenerate'):
        self.env = Environment(loader=PackageLoader(app_name, template_dir))

    def load_template(self, template: str):
        self.template = self.env.get_template(template)
        return self.template

    def render_supervised_learning(self, modules, params, hyperparams, metadata, getparams, setparams,
                                   class_name):
        return self.template.render(
            modules=modules,
            params=params,
            hyperparams=hyperparams,
            metadata=metadata,
            getparams=getparams,
            setparams=setparams,
            class_name=class_name
        )

    def render(self, **kwargs):
        return self.template.render(kwargs)


class ParseCode:

    def parse(self, *, python_path: str):
        path_elements = python_path.split(".")

    pass
